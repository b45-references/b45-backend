<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('img_url'))
{
    function img_url($url = NULL)
    {
        $link = ($url)? $url.'/' : '';
        return base_url('public/img').'/'.$link;
    }
}
