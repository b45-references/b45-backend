<div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="<?=img_url()?>prof.jpg" alt="" width="72" height="72">
        <h2><?=$title?></h2>
        <p class="lead">Below is an example of Filtering data with Datepicker using Datatables!</p>
      </div>

      <div class="row">
        <div class="col-md-8 offset-md-2">
          <h4 class="mb-3">Filtering</h4>
          <div class="row input-daterange">
              <div class="col-md-6 mb-3">
                <label for="firstName">Tanggal Mulai</label>
                <input type="text" class="form-control" id="start_date" name="start_date" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6">
                <label for="lastName">Tanggal Selesai</label>
                <input type="text" class="form-control" id="end_date" name="end_date" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
          </div>
          <div class="col-md-2 offset-md-10">
            <input type="button" name="search" id="search" value="Search" class="btn btn-info form-control"/>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <h4 class="mb-3">Data</h4>
          <table id="order_data" class="table table-bordered table-striped">
            <thead>
              <tr>
              <th>ID</th>
              <th>Nama Buku</th>
              <th>Mulai Pinjam</th>
              <th>Selesai Pinjam</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $data) { ?>
                <tr>
                  <td><?=$data->id?></td>
                  <td><?=$data->nama_buku?></td>
                  <td><?=$data->start_date?></td>
                  <td><?=$data->end_date?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">Arhen03 | &copy; 2017-2018 Upana Studio</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="https://www.upanastudio.com" target="_blank">https://www.upanastudio.com</a></li>
        </ul>
      </footer>
    </div>