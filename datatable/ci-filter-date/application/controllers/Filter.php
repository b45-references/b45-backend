<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        //Dapatkan semua data didatabase;
        $data_buku = $this->Buku->getAllData("data");
        $data = array(
			'title' => 'Tabel Data Buku Dipinjam', 
			'data'  =>  $data_buku,
            'isi'  => 'Dashboard/Filter');
        $this->load->view('Layout/Template', $data);
	}

	public function search(){
		$valid = $this->form_validation;
		$valid->set_error_delimiters('<i style="color: red;">', '</i>');
        $valid->set_rules('start_date', 'Field Start Date', 'required|trim|strip_tags|htmlspecialchars');
		$valid->set_rules('end_date', 'Field Start Date', 'required|trim|strip_tags|htmlspecialchars');
		
		if ($valid->run() === TRUE)
        {
			$input = $this->input->post(NULL, TRUE);
			$data = $this->Buku->filter("data",$input["start_date"], $input["end_date"]);
			return $this->response([
                    'data'      => array_values($data)
        	]);
		} else return  $this->response(['success' => FALSE, 'error' => validation_errors()]);
	}

	public function response($data)
    {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit();
    }
}
