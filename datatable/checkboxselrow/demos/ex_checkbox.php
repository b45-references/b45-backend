<script src="">
    $(document).ready(function () {
    
            $('#table').DataTable({
                "searching": false
            });
            $('#table').DataTable().destroy();
            //datatables
            $('#table').DataTable({
                "lengthChange": false,
                "pageLength": 20,
                "searching": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering":false,
                "autoWidth": true,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('backend/inventory/Inventory/ajax_list_inventory_bahanbaku'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.start_date = window.periodeAwal;
                        data.end_date = window.periodeAkhir;
                        data.periodeAwal = window.periodeAwal;
                        data.periodeAkhir = window.periodeAkhir;
                        data.lokasi = $('#lBarang').val();
                        data.melalui = $('#melalui').val();
                    }
                },
    
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "checkboxes": {
                        "selectRow": true
                    },
                    "orderable": false, //set not orderable
                }
                ],
                "select": {
                    "style": "multi"
                }
    
                // Set column definition initialisation properties.
                // "columns": [
                //     {
                //         "checkboxes": {
                //             "selectRow": true
                //         }
                //     }, 
                //     {
                //         "targets": [0],
                //         className:'dt-body-center'
                //     }, {
                //         "data": "1",
                //         className:'dt-body-center'
                //     }, {
                //         "data": "2"
                //     }, {
                //         "data": "3"
                //     }, {
                //         "data": "4"
                //     }, {
                //         "data": "5"
                //     }, {
                //         "data": "6"
                //     }, {
                //         "data": "7"
                //     }, {
                //         "data": "8" 
                //     }, {
                //         "data": "9"
                //     }, {
                //         "data": "10"
                //     }, {
                //         "data": "11"
                //     }, {
                //         "data": "12"
                //     }, {
                //         "data": "13"
                //     }
                // ]         
            });
    
            $('#bahanBaku-tab').click(function(){
                $('#table').DataTable().destroy();
                //datatables
                $('#table').DataTable({
                "lengthChange": false,
                "pageLength": 20,
                "searching": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering":false,
                "autoWidth": true,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('backend/inventory/Inventory/ajax_list_inventory_bahanbaku'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.start_date = window.periodeAwal;
                        data.end_date = window.periodeAkhir;
                        data.periodeAwal = window.periodeAwal;
                        data.periodeAkhir = window.periodeAkhir;
                        data.lokasi = $('#lBarang').val();
                        data.melalui = $('#melalui').val();
                    }
                },
                //Set column definition initialisation properties.
                    "columns": [
                        {
                            "targets": [0],
                            className:'dt-body-center'
                        }, {
                            "data": "1"
                        }, {
                            "data": "2"
                        }, {
                            "data": "3"
                        }, {
                            "data": "4"
                        }, {
                            "data": "5"
                        }, {
                            "data": "6"
                        }, {
                            "data": "7"
                        }, {
                            "data": "8" 
                        }, {
                            "data": "9"
                        }, {
                            "data": "10"
                        }, {
                            "data": "11"
                        }, {
                            "data": "12"
                        }
                    ]         
                });
            });
    
            $('#barangSisa-tab').click(function(){
                $('#table').DataTable().destroy();
                //datatables
                $('#table').DataTable({
                "lengthChange": false,
                "pageLength": 20,
                "searching": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering":false,
                "autoWidth": true,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('backend/inventory/Inventory/ajax_list_inventory_barangsisa'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.start_date = window.periodeAwal;
                        data.end_date = window.periodeAkhir;
                        data.periodeAwal = window.periodeAwal;
                        data.periodeAkhir = window.periodeAkhir;
                        data.lokasi = $('#lBarang').val();
                        data.melalui = $('#melalui').val();
                    }
                },
                //Set column definition initialisation properties.
                    "columns": [
                        {
                            "targets": [0],
                            className:'dt-body-center'
                        }, {
                            "data": "1"
                        }, {
                            "data": "2"
                        }, {
                            "data": "3"
                        }, {
                            "data": "4"
                        }, {
                            "data": "5"
                        }, {
                            "data": "6"
                        }, {
                            "data": "7"
                        }, {
                            "data": "8" 
                        }, {
                            "data": "9"
                        }, {
                            "data": "10"
                        }, {
                            "data": "11"
                        }, {
                            "data": "12"
                        }
                    ]         
                });
            });
            $('#barangJadi-tab').click(function(){
                $('#table').DataTable().destroy();
                //datatables
                $('#table').DataTable({
                "lengthChange": false,
                "pageLength": 20,
                "searching": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering":false,
                "autoWidth": true,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('backend/inventory/Inventory/ajax_list_inventory_barangjadi'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.start_date = window.periodeAwal;
                        data.end_date = window.periodeAkhir;
                        data.periodeAwal = window.periodeAwal;
                        data.periodeAkhir = window.periodeAkhir;
                        data.lokasi = $('#lBarang').val();
                        data.melalui = $('#melalui').val();
                    }
                },
                //Set column definition initialisation properties.
                    "columns": [
                        {
                            "targets": [0],
                            className:'dt-body-center'
                        }, {
                            "data": "1"
                        }, {
                            "data": "2"
                        }, {
                            "data": "3"
                        }, {
                            "data": "4"
                        }, {
                            "data": "5"
                        }, {
                            "data": "6"
                        }, {
                            "data": "7"
                        }, {
                            "data": "8" 
                        }, {
                            "data": "9"
                        }, {
                            "data": "10"
                        }, {
                            "data": "11"
                        }, {
                            "data": "12"
                        }
                    ]         
                });
            });
            $('#WIP-tab').click(function(){
                $('#table').DataTable().destroy();
                //datatables
                $('#table').DataTable({
                "lengthChange": false,
                "pageLength": 20,
                "searching": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering":false,
                "autoWidth": true,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('backend/inventory/Inventory/ajax_list_inventory_wip'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.start_date = window.periodeAwal;
                        data.end_date = window.periodeAkhir;
                        data.periodeAwal = window.periodeAwal;
                        data.periodeAkhir = window.periodeAkhir;
                        data.lokasi = $('#lBarang').val();
                        data.melalui = $('#melalui').val();
                    }
                },
                //Set column definition initialisation properties.
                    "columns": [
                        {
                            "targets": [0],
                            className:'dt-body-center'
                        }, {
                            "data": "1"
                        }, {
                            "data": "2"
                        }, {
                            "data": "3"
                        }, {
                            "data": "4"
                        }, {
                            "data": "5"
                        }, {
                            "data": "6"
                        }, {
                            "data": "7"
                        }, {
                            "data": "8" 
                        }, {
                            "data": "9"
                        }, {
                            "data": "10"
                        }, {
                            "data": "11"
                        }, {
                            "data": "12"
                        }
                    ]         
                });
            });
        });
</script>
